# zstd --help
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-t","--target",type=str,help="target .zst file",requires=True)\
args = parser.parse_args()
target = args.target
assert target.endswith(".zst")
import shutil
def trycatch(f):
    try:
        f()
    except:
        pass
rdump = lambda: shutil.rmtree("dump")
zstd -D dump -d @(target)